package com.example.guyapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guyapplication.model.Comment;
import com.example.guyapplication.model.Model;
import com.example.guyapplication.model.Question;
import com.example.guyapplication.model.Topic;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;


public class QuestionActivity extends AppCompatActivity {
    private Question question;
    private FloatingActionButton addComment;
    private ListView commentsListView;
    private CommentsAdapter adapter;
    private String topicName;
    private String subjectName;
    private String questioncontent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        initWidgets();
    }

    private void initWidgets() {
        this.questioncontent = getIntent().getExtras().getString(getString(R.string.question_id_key));
        this.subjectName = getIntent().getExtras().getString("subject");
        this.topicName = getIntent().getExtras().getString("topic");
        addComment = findViewById(R.id.add_comment);
        addComment.setOnClickListener(view -> createDialog());
        this.question =  new Question(questioncontent,questioncontent);
        TextView titleTextView = findViewById(R.id.title_text_view);
        titleTextView.setText(this.question.getTitle());
        this.commentsListView = (ListView) findViewById(R.id.comments_list_view);
        this.adapter = new CommentsAdapter(this, 0, 0, new ArrayList<>());
        updateComments();
        this.commentsListView.setAdapter(adapter);
        this.commentsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            // @SuppressLint("StringFormatInvalid")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    private void updateComments() {
        // used to get the latest list of comments
        List<Comment> comments = Model.getInstance().getComments(subjectName, topicName, questioncontent);
        this.adapter.setComments(comments);
    }

    private void createDialog() {
        // dialog of add comment
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.add_comment_dialog, null);
        dialogBuilder.setView(dialogView);
        EditText editText = (EditText) dialogView.findViewById(R.id.addCommentDialog);
        dialogBuilder.setPositiveButton("add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String data = editText.getText().toString();
                boolean add = Model.getInstance().add_comment(questioncontent, subjectName, topicName, data);
                if (add) {
                    updateComments();
                    adapter.notifyDataSetChanged();
                    Toast.makeText(QuestionActivity.this, "Add", Toast.LENGTH_LONG).show();
                }
            }
        })
                .setNegativeButton("Cancel", (dialog, id) -> {
                    Toast.makeText(QuestionActivity.this, "Cancel", Toast.LENGTH_LONG).show();
                    ;
                });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}
