package com.example.guyapplication.model;

import android.content.Intent;
import android.text.method.Touch;
import android.util.Log;
import android.widget.Toast;

import com.example.guyapplication.Client;
import com.example.guyapplication.SubjectsListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class Model {
    private static Model instance = new Model();
    private final List<Subject> subjects;

    private Model() {
        List<String> subjectsStrings = Arrays.asList("מתמטיקה", "מדעי המחשב", "עברית");
        this.subjects = new ArrayList<Subject>();
        for (String subjectsNames : subjectsStrings) {
            this.subjects.add(new Subject(subjectsNames));
        }
    }

    public static Model getInstance() {
        return instance;
    }

    private static <T> List<T> jsonArrayToList(JSONArray arr) {
        ArrayList<T> list = new ArrayList<T>();
        try {
            for (int i = 0, l = arr.length(); i < l; i++) {
                list.add((T) arr.get(i));
            }
        } catch (JSONException e) {
        }
        return list;
    }
    public List<Subject> getSubjects(String username) {
        // get username and send request to the server to get the list of the subjects the user belong to, use sendrequset function
        try {
            JSONObject sendingData = new JSONObject();
            sendingData.put("request", "subjects_list");
            sendingData.put("username", username);
            Client dataTransportTask = new Client(sendingData);
            JSONObject received = dataTransportTask.execute().get();
            List<String> subjectsNames = jsonArrayToList(received.getJSONArray("subjects"));
            List<Subject> subjects = new ArrayList<>();
            for (String subjectName : subjectsNames) {
                subjects.add(new Subject(subjectName));
            }
            return subjects;
        } catch (JSONException | InterruptedException | ExecutionException e) {
            Log.e("Exception", e.toString());
        }
        return null;
    }

    public boolean removeSubject(String subject, String username) {
        //get username and subject and send request to the server with sendreqeust function  to leave the group response if it's work
        Map<String, Object> data = new HashMap<>();
        data.put("username", username);
        data.put("subject", subject);
        return sendRequest("leave_group", data, "left");
    }

 // public Subject getSubjectById(String subjectId) {
 //     //
 //     for (Subject subject : this.subjects) {
 //         if (subject.getId().equals(subjectId))
 //             return subject;
 //     }
 //     return null;
 // }




    public boolean register(User user) {
        // get user info (username, password) and send request to the server with sendreqeust function to register the user and response if it's work
        Map<String, Object> data = new HashMap<>();
        data.put("username", user.getUsername());
        data.put("password", user.getPassword());
        return sendRequest("register", data, "registration succeed");
    }
    public boolean leaveGroup(String username,String subject) {
        // get username and subject and end request to the server with sendreqeust function to leave the group and response if it's work
        Map<String, Object> data = new HashMap<>();
        data.put("username", username);
        data.put("subject", subject);
        return sendRequest("leave_group", data, "left");
    }

    public boolean login(User user) {
        // get user info (username, password) and send request to the server with sendreqeust function to cheek if there is user with this info and response  according to.
        Map<String, Object> data = new HashMap<>();
        data.put("username", user.getUsername());
        data.put("password", user.getPassword());
        return sendRequest("login", data, "user found");
    }

    private static boolean sendRequest(String route, Map<String, Object> data, String response) {
        //get the request and a Dictionary of the info to send to the server and the desired response and at the end response if the server response desired response
        try {
            JSONObject sendingData = new JSONObject();
            sendingData.put("request", route);
            Set<Map.Entry<String, Object>> entry = data.entrySet();
            for (Map.Entry<String, Object> stringObjectEntry : entry) {
                sendingData.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
            }
            Client dataTransportTask = new Client(sendingData);
            JSONObject received = dataTransportTask.execute().get();
            if (received.get("response").toString().equals(response)) {
                return true;
            }
        } catch (JSONException | InterruptedException | ExecutionException e) {
            Log.e("Exception", e.toString());
        }
        return false;
    }

    public List<Topic> getSubjectTopics(String subject) {
        //get subject and send request to the server with sendreqeust function to get list of the topics in subject
        try {
            JSONObject sendingData = new JSONObject();
            sendingData.put("request", "topics_list");
            sendingData.put("subject", subject);
            Client dataTransportTask = new Client(sendingData);
            JSONObject received = dataTransportTask.execute().get();
            List<String> topicNames = jsonArrayToList(received.getJSONArray("topics"));
            List<Topic> topics = new ArrayList<>();
            for (String topicName : topicNames) {
                topics.add(new Topic(topicName));
            }
            return topics;
        } catch (JSONException | InterruptedException | ExecutionException e) {
            Log.e("Exception", e.toString());
        }
        return null;
    }

    public List<Comment> getComments(String subject, String topic, String question) {
        //get subject and topic and question and send request to the server with sendreqeust function to get list of the comment in subject
        try {
            JSONObject sendingData = new JSONObject();
            sendingData.put("request", "comments_list");
            sendingData.put("subject", subject);
            sendingData.put("topic", topic);
            sendingData.put("question", question);
            Client dataTransportTask = new Client(sendingData);
            JSONObject received = dataTransportTask.execute().get();
            List<String> commentNames = jsonArrayToList(received.getJSONArray("comments"));
            List<Comment> comments = new ArrayList<>();
            for (String commentName : commentNames) {
                comments.add(new Comment(commentName));
            }
            return comments;
        } catch (JSONException | InterruptedException | ExecutionException e) {
            Log.e("Exception", e.toString());
        }
        return null;
    }


    public List<Question> getQuestions(String subject, String topic) {
        //get subject and topic and send request to the server with sendreqeust function to get list of the questions in topic
        try {
            JSONObject sendingData = new JSONObject();
            sendingData.put("request", "questions_list");
            sendingData.put("subject", subject);
            sendingData.put("topic", topic);
            Client dataTransportTask = new Client(sendingData);
            JSONObject received = dataTransportTask.execute().get();
            List<String> questionNames = jsonArrayToList(received.getJSONArray("questions"));
            List<Question> questions = new ArrayList<>();
            for (String questionName : questionNames) {
                questions.add(new Question(questionName, questionName));
            }
            System.out.println(questions);
            return questions;
        } catch (JSONException | InterruptedException | ExecutionException e) {
            Log.e("Exception", e.toString());
        }
        return null;
    }
    public boolean add_question(String question,String subject,String topic) {
        //get subject and topic and question and send request to the server with sendreqeust function to add question.
        Map<String, Object> data = new HashMap<>();
        data.put("question", question);
        data.put("subject", subject);
        data.put("topic", topic);
        return sendRequest("add_question", data, "question added");
    }
    public boolean add_comment(String question,String subject,String topic,String comment) {
        //get subject and topic and question and comment send request to the server with sendreqeust function to add comment.
        Map<String, Object> data = new HashMap<>();
        data.put("question", question);
        data.put("subject", subject);
        data.put("topic", topic);
        data.put("comment", comment);
        return sendRequest("add_comment", data, "comment added");
    }
    public boolean add_subject(String username,String subject) {
        //get subject and username and send request to the server with sendreqeust function to add user to group of subject.
        Map<String, Object> data = new HashMap<>();
        data.put("username", username);
        data.put("subject", subject);
        return sendRequest("add_subject", data, "add to group");
    }


}
