package com.example.guyapplication.model;

public class Comment {
    private String id, content;

    public Comment() {
        this.id = Utils.randomId();
    }

    public Comment(String content) {
        this.id = Utils.randomId();
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
