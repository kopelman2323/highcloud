package com.example.guyapplication.model;

public class Subject {
    private String id, name;

    public Subject() {
        this.id = Utils.randomId();
    }

    public Subject(String name) {
        this.id = Utils.randomId();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}