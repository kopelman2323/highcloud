package com.example.guyapplication.model;

import java.util.UUID;

public class Utils {
    public static String randomId() {
        return UUID.randomUUID().toString();
    }
}
