package com.example.guyapplication.model;

public class Topic {
    private String id, name;

    public Topic() {
        this.id = Utils.randomId();
    }

    public Topic(String name) {
        this.id = Utils.randomId();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}