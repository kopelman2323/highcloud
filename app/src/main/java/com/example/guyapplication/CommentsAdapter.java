package com.example.guyapplication;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.guyapplication.model.Comment;
import com.example.guyapplication.model.Question;

import java.util.List;

public class CommentsAdapter extends ArrayAdapter<Comment> {
    private final Bitmap groupLogo;
    private Activity activity;
    private List<Comment> comments;

    public CommentsAdapter(Activity activity, int resource, int textViewResourceId, List<Comment> comments) {
        super(activity,resource, textViewResourceId, comments);
        this.activity = activity;
        this.comments = comments;
        this.groupLogo = BitmapFactory.decodeResource(activity.getResources(), R.drawable.group_logo);
    }
    public void setComments(List<Comment> comments) {
        // get a list of comments clear her than build it again with the new changes
        this.comments.clear();
        this.comments.addAll(comments);
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
           LayoutInflater layoutInflater = this.activity.getLayoutInflater();
           convertView = layoutInflater.inflate(R.layout.subject_row_layout, parent, false);
        }
        TextView topicTextView = convertView.findViewById(R.id.subject_text_view);
        ImageView logoImageView = convertView.findViewById(R.id.app_logo_image_view);
        Comment comment = this.comments.get(position);
        logoImageView.setImageBitmap(this.groupLogo);
        topicTextView.setText(comment.getContent());
        return convertView;
    }
}