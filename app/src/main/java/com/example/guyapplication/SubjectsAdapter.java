package com.example.guyapplication;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.guyapplication.model.Question;
import com.example.guyapplication.model.Subject;

import java.util.List;

public class SubjectsAdapter extends ArrayAdapter<Subject> {
    private final Bitmap groupLogo;
    private Activity activity;
    private List<Subject> subjects;

    public SubjectsAdapter(Activity activity, int resource, int textViewResourceId, List<Subject> subjects) {
        super(activity, resource, textViewResourceId, subjects);
        this.activity = activity;
        this.subjects = subjects;
        this.groupLogo = BitmapFactory.decodeResource(activity.getResources(), R.drawable.group_logo);
    }
    public void setSubjects(List<Subject> subjects) {
        // get a list of subjects clear her than build it again with the new changes
        this.subjects.clear();
        this.subjects.addAll(subjects);
        this.notifyDataSetChanged();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = activity.getLayoutInflater();
            convertView = layoutInflater.inflate(R.layout.subject_row_layout, parent, false);
        }
        TextView subjectTextView = convertView.findViewById(R.id.subject_text_view);
        ImageView logoImageView = convertView.findViewById(R.id.app_logo_image_view);
        Subject subject = this.subjects.get(position);
        logoImageView.setImageBitmap(this.groupLogo);
        subjectTextView.setText(subject.getName());
        return convertView;
    }
}