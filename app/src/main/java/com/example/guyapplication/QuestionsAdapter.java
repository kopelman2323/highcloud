package com.example.guyapplication;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.guyapplication.model.Question;
import com.example.guyapplication.model.Topic;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class QuestionsAdapter extends ArrayAdapter<Question> {
    private final Bitmap groupLogo;
    private Activity activity;
    private List<Question> questions;

    public QuestionsAdapter(Activity activity, int resource, int textViewResourceId, List<Question> questions) {
        super(activity, resource, textViewResourceId, questions);
        this.activity = activity;
        this.questions = questions;
        this.groupLogo = BitmapFactory.decodeResource(activity.getResources(), R.drawable.group_logo);
    }

    public void setQuestions(List<Question>questions) {
        // get a list of questions clear her than build it again with the new changes
        this.questions.clear();
        this.questions.addAll(questions);
        this.notifyDataSetChanged();
    }

    public Question getQuestion(int index) {
        return questions.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = activity.getLayoutInflater();
            convertView = layoutInflater.inflate(R.layout.subject_row_layout, parent, false);
        }
        TextView topicTextView = convertView.findViewById(R.id.subject_text_view);
        ImageView logoImageView = convertView.findViewById(R.id.app_logo_image_view);
        Question question = this.questions.get(position);
        logoImageView.setImageBitmap(this.groupLogo);
        topicTextView.setText(question.getTitle());
        return convertView;
    }
}
