package com.example.guyapplication;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.guyapplication.model.Topic;

import java.util.List;

public class TopicsAdapter extends ArrayAdapter<Topic> {
    private final Bitmap groupLogo;
    private Activity activity;
    private List<Topic> topics;

    public TopicsAdapter(Activity activity, int resource, int textViewResourceId, List<Topic> topics) {
        super(activity, resource, textViewResourceId, topics);
        this.activity = activity;
        this.topics = topics;
        this.groupLogo = BitmapFactory.decodeResource(activity.getResources(), R.drawable.group_logo);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = activity.getLayoutInflater();
            convertView = layoutInflater.inflate(R.layout.subject_row_layout, parent, false);
        }
        TextView topicTextView = convertView.findViewById(R.id.subject_text_view);
        ImageView logoImageView = convertView.findViewById(R.id.app_logo_image_view);
        Topic topic = this.topics.get(position);
        logoImageView.setImageBitmap(this.groupLogo);
        topicTextView.setText(topic.getName());
        return convertView;
    }
}