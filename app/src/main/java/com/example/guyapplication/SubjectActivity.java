package com.example.guyapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guyapplication.model.Model;
import com.example.guyapplication.model.Subject;
import com.example.guyapplication.model.Topic;

import java.util.List;

public class SubjectActivity extends AppCompatActivity {
    private Subject subject;
    private TextView titleTextView;
    private ListView topicsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);
        initWidgets();
    }

    private void initWidgets() {
        String subjectName = getIntent().getExtras().getString(getString(R.string.subject_id_key));
        this.subject = new Subject(subjectName);
        this.titleTextView = findViewById(R.id.title_text_view);
        this.titleTextView.setText(this.subject.getName());
        this.topicsListView = findViewById(R.id.topics_list_view);
        List<Topic> topics = Model.getInstance().getSubjectTopics(this.subject.getName());
        this.topicsListView.setAdapter(new TopicsAdapter(this, 0, 0, topics));
        this.topicsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SubjectActivity.this, TopicActivity.class);
                String topicName = topics.get(position).getName();
                intent.putExtra(getString(R.string.topic_id_key), topicName);
                intent.putExtra("subject", subject.getName());
                startActivity(intent);
            }
        });
    }
}