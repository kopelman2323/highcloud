package com.example.guyapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guyapplication.model.Model;
import com.example.guyapplication.model.Question;
import com.example.guyapplication.model.Topic;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class TopicActivity extends AppCompatActivity {
    private Topic topic;
    private FloatingActionButton addQuestion;
    private ListView questionsListView;
    private EditText addQuestionDialog;
    private QuestionsAdapter adapter;
    private String topicName;
    private String subjectName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);
        initWidgets();
    }

    private void initWidgets() {
        this.topicName = getIntent().getExtras().getString(getString(R.string.topic_id_key));
        this.subjectName = getIntent().getExtras().getString("subject");
        addQuestion = findViewById(R.id.add_question);
        addQuestion.setOnClickListener(view -> createDialog());
        this.topic = new Topic(topicName);
        TextView titleTextView = findViewById(R.id.title_text_view);
        titleTextView.setText(this.topic.getName());
        this.questionsListView = findViewById(R.id.questions_list_view);
        this.adapter = new QuestionsAdapter(this, 0, 0, new ArrayList<>());
        updateQuestions();
        this.questionsListView.setAdapter(adapter);
        this.questionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(TopicActivity.this, QuestionActivity.class);
                String questioncontent = adapter.getQuestion(position).getContent();
                intent.putExtra(getString(R.string.question_id_key), questioncontent);
                intent.putExtra("topic", topicName);
                intent.putExtra("subject", subjectName);
                startActivity(intent);
            }
        });
    }

    private void updateQuestions() {
        //// used to get the latest list of questions
        List<Question> questions = Model.getInstance().getQuestions(subjectName, topicName);
        this.adapter.setQuestions(questions);
    }

    private void createDialog() {
        // dialog of add comment
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.add_question_dialog, null);
        dialogBuilder.setView(dialogView);
        EditText editText = dialogView.findViewById(R.id.addQuestionDialog);
        dialogBuilder.setPositiveButton("add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String data = editText.getText().toString();
                boolean add = Model.getInstance().add_question(data, subjectName, topicName);
                if (add) {
                    updateQuestions();
                    Toast.makeText(TopicActivity.this, "Add", Toast.LENGTH_LONG).show();
                }
            }
        })
                .setNegativeButton("Cancel", (dialog, id) -> {
                    Toast.makeText(TopicActivity.this, "Cancel", Toast.LENGTH_LONG).show();
                });
        ;
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    ;

}



