package com.example.guyapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guyapplication.model.Model;
import com.example.guyapplication.model.Subject;

 import java.util.ArrayList;
import java.util.List;

public class SubjectsListActivity extends AppCompatActivity {
    private ListView subjectsListView;
    private TextView text_list;
    private SubjectsAdapter subjectsAdapter;
    private String username;
    private Button addbtn;
    private String[] subjectsAdd = {"מדעי המחשב","מתמטיקה","עברית"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects_list);
        initWidgets();
    }

    private void initWidgets() {
        this.addbtn = findViewById(R.id.addbtn);
        this.addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addGroup();
            }
        });
        this.username = getIntent().getExtras().getString("username");
        List<Subject> subjects = Model.getInstance().getSubjects(username);
        this.text_list = findViewById(R.id.text_list);
        this.subjectsAdapter = new SubjectsAdapter(this, 0, 0, new ArrayList<>());
        updateSubjects();
        this.subjectsListView = findViewById(R.id.subjects_list_view);
        this.subjectsListView.setAdapter(subjectsAdapter);
        text_list.setText(" היי " + username + " אלה הקבוצות אליהן אתה משתייך ");
        this.subjectsListView.setOnItemLongClickListener((parent, view, position, id) -> {
            createDeleteDialog(subjects.get(position), username);
            return true;
        });
        this.subjectsListView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(SubjectsListActivity.this, SubjectActivity.class);
            intent.putExtra(getString(R.string.subject_id_key), subjects.get(position).getName());
            startActivity(intent);
        });
    }

    private void updateSubjects() {
        //
        List<Subject> subjects = Model.getInstance().getSubjects(username);
        this.subjectsAdapter.setSubjects(subjects);
    }

    private void createDeleteDialog(Subject subject, String username) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format("Are you sure you want to leave '%s' group?", subject.getName()))
                .setPositiveButton("Yes, leave", (dialog, id) -> {
                    if (Model.getInstance().removeSubject(subject.getName(), username)) {
                        Toast.makeText(SubjectsListActivity.this, "Delete", Toast.LENGTH_LONG).show();
                        boolean add = Model.getInstance().leaveGroup(username, subject.getName());
                        if (add) {
                            updateSubjects();
                        } else {
                            Log.d(getString(R.string.app_name), String.format("createDeleteDialog: deletion didn't success, subject: %s", subject));
                        }
                    }
                })
                .setNegativeButton("Cancel", (dialog, id) -> {
                });
        builder.create().show();
    }

    public void addGroup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SubjectsListActivity.this);
        builder.setTitle("בחר קבוצה אליה תרצה להצטרף")
                .setItems(subjectsAdd, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        String subjectToAdd = subjectsAdd[which];
                        boolean add = Model.getInstance().add_subject(username, subjectToAdd);
                        if (add) {
                            updateSubjects();
                        }
                    }
                });
         builder.create().show();
    }
}
