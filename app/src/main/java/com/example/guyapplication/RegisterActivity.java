package com.example.guyapplication;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guyapplication.model.Model;
import com.example.guyapplication.model.User;

import java.text.BreakIterator;

public class RegisterActivity extends AppCompatActivity {
    public Button submitButton, loginButton;
    private EditText passwordEditText, usernameEditText, repeatPasswordEditText;
    private BreakIterator dataEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        initWidgets();
    }

    private void initWidgets() {
        this.submitButton = findViewById(R.id.submit_button);
        this.repeatPasswordEditText = findViewById(R.id.repeat);
        this.usernameEditText = findViewById(R.id.user_name_edit_text);
        this.passwordEditText = findViewById(R.id.password_edit_text);
        this.loginButton = findViewById(R.id.login_button);
        this.submitButton.setOnClickListener(view -> {
            if (this.passwordEditText.getText().toString().equals(this.repeatPasswordEditText.getText().toString())) {
                User user = generateUser();
                if (!Model.getInstance().register(user)) {
                    Toast.makeText(this, "Registration failed", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(this, SubjectsListActivity.class);
                    intent.putExtra("username", user.getUsername());
                    startActivity(intent);
                }
            }
            else {
                passwordEditText.setError("password do not match");
                repeatPasswordEditText.setError("password do not match");
            }
        });
        this.loginButton.setOnClickListener(view -> startActivity(new Intent(this, LoginActivity.class)));
    }

    private User generateUser() {
        // build user object to make it easier to send to the server side
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        return new User(username, password);
    }
}
