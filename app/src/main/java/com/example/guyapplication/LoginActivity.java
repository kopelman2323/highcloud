package com.example.guyapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.guyapplication.model.Model;
import com.example.guyapplication.model.User;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Button loginButton;
    private EditText UserNameEditText, PasswordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        initWidgets();
    }

    private void initWidgets() {
        this.PasswordEditText = findViewById(R.id.password_edit_text);
        this.UserNameEditText = findViewById(R.id.user_name_edit_text);
        loginButton = findViewById(R.id.login_button);
        this.loginButton.setOnClickListener(view -> {
            User user = generateUser();
            if (!Model.getInstance().login(user)) {
                Toast.makeText(this, "password or username worng", Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(this, SubjectsListActivity.class);
                intent.putExtra("username", user.getUsername());
                startActivity(intent);
            }
        });
    }

    private User generateUser() {
        String username = UserNameEditText.getText().toString();
        String password = PasswordEditText.getText().toString();
        return new User(username, password);
    }

    @Override
    public void onClick(View v) {

    }
}