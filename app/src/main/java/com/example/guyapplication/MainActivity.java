package com.example.guyapplication;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private TextView titleTextView;
    private Button loginButton, registerButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initWidgets();
    }

    private void initWidgets() {
        this.titleTextView = findViewById(R.id.title_text_view);
        this.titleTextView.setText(String.format("ברוכים הבאים לאפליקציית %s", getString(R.string.app_name)));
        this.loginButton = findViewById(R.id.login_button);
        this.registerButton = findViewById(R.id.toregisterbtn);
        this.loginButton.setOnClickListener(view -> startActivity(new Intent(this, LoginActivity.class)));
        this.registerButton.setOnClickListener(view -> startActivity(new Intent(this, RegisterActivity.class)));
    }
}

